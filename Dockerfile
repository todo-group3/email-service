FROM golang:1.19.3-alpine
RUN mkdir email
COPY . /email
WORKDIR /email
RUN go build -o main cmd/main.go
CMD ./main