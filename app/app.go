package app

import (
	"encoding/json"
	"fmt"

	"github.com/streadway/amqp"
	"gitlab.com/todo-group/email-service/config"
	"gitlab.com/todo-group/email-service/email"
	"gitlab.com/todo-group/email-service/models"
	"gitlab.com/todo-group/email-service/pkg/logger"
	"gitlab.com/todo-group/email-service/pkg/rabbitmq"
)

type App struct {
	EmailSender   *email.EmailSender
	MessageBroker *rabbitmq.RabbitMQ
	Log           *logger.Logger
	Cfg           *config.Config
}

func New(log *logger.Logger, cfg *config.Config) (*App, error) {
	mBroker, err := rabbitmq.NewRabbitMQ(cfg)
	if err != nil {
		return &App{}, err
	}

	return &App{
		EmailSender:   email.New(cfg, log),
		MessageBroker: mBroker,
		Log:           log,
		Cfg:           cfg,
	}, nil
}

func (a *App) VerificationConsumer(cfg *config.Config, log *logger.Logger) {
	var ch *amqp.Channel
	var err error
	for {
		ch, err = a.MessageBroker.Conn.Channel()
		if err == nil {
			break
		}
	}
	defer ch.Close()

	msgs, err := ch.Consume(
		a.Cfg.VerifyTopic,
		"verify",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		a.Log.Error("Please restart the server")
	}

	a.Log.Info("Verification consumer is waiting for messages on topic " + cfg.VerifyTopic)
	body := &models.Verification{}
	for d := range msgs {
		err := json.Unmarshal(d.Body, &body)
		if err != nil {
			a.Log.Error(fmt.Errorf("app verification unmarshal: %w", err))
			continue
		}
		err = a.EmailSender.SendEmailVerification(body)
		if err != nil {
			a.Log.Error(fmt.Errorf("app verification sendemail: %w", err))
		}
	}
}

func (a *App) ReminderConsumer(cfg *config.Config, log *logger.Logger) {
	var ch *amqp.Channel
	var err error
	for {
		ch, err = a.MessageBroker.Conn.Channel()
		if err == nil {
			break
		}
	}
	defer ch.Close()

	msgs, err := ch.Consume(
		a.Cfg.RemainderTopic,
		"remind",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		a.Log.Error("Please restart the service")
	}

	a.Log.Info("Remainder consumer is waiting for messages on topic " + cfg.RemainderTopic)
	body := &models.RemainderReq{}
	for d := range msgs {
		err := json.Unmarshal(d.Body, &body)
		if err != nil {
			a.Log.Error(fmt.Errorf("json unmarshal: %w", err))
			continue
		}
		if body.Type == cfg.RemainderType {
			err = a.EmailSender.SendRemainder(body)
			if err != nil {
				a.Log.Error(fmt.Errorf("app if remaindertype: %w", err))
			}
		} else if body.Type == cfg.MissRemainderType {
			err = a.EmailSender.SendMissRemainder(body)
			if err != nil {
				a.Log.Error(fmt.Errorf("app if missremaindertype: %w", err))
			}
		}
	}
}
