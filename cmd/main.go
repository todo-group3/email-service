package main

import (
	"fmt"
	"sync"

	"gitlab.com/todo-group/email-service/app"
	"gitlab.com/todo-group/email-service/config"
	"gitlab.com/todo-group/email-service/pkg/logger"
)

func main() {
	cfg := config.New()
	log := logger.New(cfg.LogLevel)

	app, err := app.New(log, cfg)
	if err != nil {
		log.Fatal(fmt.Errorf("main  main app: %w", err))
		return
	}
	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		app.ReminderConsumer(cfg, log)
		wg.Done()
	}()
	go func() {
		app.VerificationConsumer(cfg, log)
		wg.Done()
	}()
	wg.Wait()
}
