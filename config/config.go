package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	RabbitMQHost              string
	RabbitMQPort              string
	RabbitMQUser              string
	RabbitMQPassword          string
	RabbitMQConnectionTry     int
	RabbitMQConnectionTimeOut int
	EmailPassword             string
	Email                     string
	VerifyTopic               string
	RemainderTopic            string
	RemainderType             string
	MissRemainderType         string
	LogLevel                  string
}

func New() *Config {
	c := &Config{}

	c.Email = cast.ToString(GetOrReturnDefault("EMAIL", "testname640@gmail.com"))
	c.EmailPassword = cast.ToString(GetOrReturnDefault("EMAIL_PASSWORD", "234FSDFASF"))

	c.RabbitMQConnectionTimeOut = cast.ToInt(GetOrReturnDefault("RABBIT_MQ_CONNECTION_TIME_OUT", 5))
	c.RabbitMQConnectionTry = cast.ToInt(GetOrReturnDefault("RABBIT_MQ_CONNECTION_TRY", 100))
	c.LogLevel = cast.ToString(GetOrReturnDefault("LOG_LEVEL", "debug"))

	c.RabbitMQHost = cast.ToString(GetOrReturnDefault("RABBIT_MQ_HOST", "localhost"))
	c.RabbitMQPort = cast.ToString(GetOrReturnDefault("RABBIT_MQ_PORT", "5672"))
	c.RabbitMQUser = cast.ToString(GetOrReturnDefault("RABBIT_MQ_USER", "azizbek"))
	c.RabbitMQPassword = cast.ToString(GetOrReturnDefault("RABBIT_MQ_USER", "azizbek"))
	c.RemainderTopic = cast.ToString(GetOrReturnDefault("REMAINDER_TOPIC", "remind"))
	c.VerifyTopic = cast.ToString(GetOrReturnDefault("VERIFY_TOPIC", "verify"))
	c.RemainderType = cast.ToString(GetOrReturnDefault("REMAINDER_TYPE", "remind"))
	c.MissRemainderType = cast.ToString(GetOrReturnDefault("MISS_REMAINDER_TYPE", "miss_remind"))
	return c
}

func GetOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
