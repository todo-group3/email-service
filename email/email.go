package email

import (
	"bytes"
	"fmt"
	"net/smtp"
	"text/template"

	"gitlab.com/todo-group/email-service/config"
	"gitlab.com/todo-group/email-service/models"
	"gitlab.com/todo-group/email-service/pkg/logger"
)

type EmailSender struct {
	Log *logger.Logger
	Cfg *config.Config
}

func New(cfg *config.Config, log *logger.Logger) *EmailSender {
	return &EmailSender{
		Log: log,
		Cfg: cfg,
	}
}

func (e *EmailSender) SendEmailVerification(req *models.Verification) error {
	body := new(bytes.Buffer)

	t, err := template.ParseFiles("./email/email_temp/verify.html")
	if err != nil {
		e.Log.Error("Error while parsing html template", err)
		return err
	}
	
	t.Execute(body, req)
	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	msg := []byte("Subject: Welcome to todo app\n" + mime + body.String())


	auth := smtp.PlainAuth("", e.Cfg.Email, e.Cfg.EmailPassword, "smtp.gmail.com")

	err = smtp.SendMail("smtp.gmail.com:587", auth, e.Cfg.Email, []string{req.Email}, msg)
	if err != nil {
		e.Log.Error("Error while sending email", err)
		return err
	}
	e.Log.Info(fmt.Sprintf("Messages is sent to %s", req.Email))

	return nil
}

func (e *EmailSender) SendRemainder(req *models.RemainderReq) error {
	body := new(bytes.Buffer)

	t, err := template.ParseFiles("./email/email_temp/remind.html")
	if err != nil {
		e.Log.Error("Error while parsing html template", err)
		return err
	}

	t.Execute(body, req)

	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	msg := []byte("Subject: To Do Remainder\n" + mime + body.String())

	auth := smtp.PlainAuth("", e.Cfg.Email, e.Cfg.EmailPassword, "smtp.gmail.com")

	err = smtp.SendMail("smtp.gmail.com:587", auth, e.Cfg.Email, []string{req.Email}, msg)
	if err != nil {
		e.Log.Error("Error while sending email", err)
		return err
	}
	e.Log.Info(fmt.Sprintf("Messages is sent to %s", req.Email))
	return nil
}

func (e *EmailSender) SendMissRemainder(req *models.RemainderReq) error {
	body := new(bytes.Buffer)

	t, err := template.ParseFiles("./email/email_temp/miss.html")
	if err != nil {
		e.Log.Error("Error while parsing html template", err)
		return err
	}
	t.Execute(body, req)

	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	msg := []byte("Subject: Missed task\n" + mime + body.String())


	auth := smtp.PlainAuth("", e.Cfg.Email, e.Cfg.EmailPassword, "smtp.gmail.com")

	err = smtp.SendMail("smtp.gmail.com:587", auth, e.Cfg.Email, []string{req.Email}, msg)
	if err != nil {
		e.Log.Error("Error while sending email", err)
		return err
	}
	e.Log.Info(fmt.Sprintf("Messages is sent to %s", req.Email))
	return nil
}
