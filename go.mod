module gitlab.com/todo-group/email-service

go 1.19

require (
	github.com/rs/zerolog v1.28.0
	github.com/spf13/cast v1.5.0
	github.com/streadway/amqp v1.0.0
)

require (
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/sys v0.0.0-20210927094055-39ccf1dd6fa6 // indirect
)
