package models

type Verification struct {
	FirstName string `json:"first_name"`
	Email     string `json:"email"`
	Code      string `json:"code"`
}

type RemainderReq struct {
	FirstName string `json:"first_name"`
	Email     string `json:"email"`
	Title     string `json:"title"`
	Type      string `json:"type"`
	Start     string `json:"start"`
	End       string `json:"end"`
}
