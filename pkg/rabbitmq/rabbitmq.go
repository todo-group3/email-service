package rabbitmq

import (
	"errors"
	"fmt"
	"time"

	"github.com/streadway/amqp"
	"gitlab.com/todo-group/email-service/config"
)

type RabbitMQ struct {
	Conn *amqp.Connection
}

func NewRabbitMQ(cfg *config.Config) (*RabbitMQ, error) {
	// connection string := amqp://user:password@host:port/
	conStr := fmt.Sprintf("amqp://%s:%s@%s:%s/",
		cfg.RabbitMQUser,
		cfg.RabbitMQPassword,
		cfg.RabbitMQHost,
		cfg.RabbitMQPort,
	)
	fmt.Println(conStr)
	fmt.Println("trying to connect to RabbitMQ")
	for cfg.RabbitMQConnectionTry > 0 {
		conn, err := amqp.Dial(conStr)
		if err == nil {
			fmt.Println("Successfully connected to Rabbitmq")
			return &RabbitMQ{
				Conn: conn,
			}, nil
		} 
		time.Sleep(time.Second * time.Duration(cfg.RabbitMQConnectionTimeOut))
		cfg.RabbitMQConnectionTry--
	}
	return &RabbitMQ{}, errors.New("connection timeout")
}
